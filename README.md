Run the Application
------------------


to run the application simply open the `index.html` file in your browser.


Licence
-------

The Application is under the MIT License. See LICENSE.txt
